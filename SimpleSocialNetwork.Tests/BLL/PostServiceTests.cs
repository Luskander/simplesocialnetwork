﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Services;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.Tests.BLL
{
	[TestClass]
	public class PostServiceTests
	{
		[TestMethod]
		public async Task PostService_CreatePost_WorksCorrectly()
		{
			//Arrange
			var post = new PostDTO
			{
				Id = 1,
				OwnerId = GetTestProfile().Id,
				IsDeleted = false,
				Content = "Content",
				CreatedAt = DateTime.Now,
				Title = "Test"
			};

			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.Posts.GetAll()).Returns(GetTestPosts());
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestProfiles());
			var postService = new PostService(mockUnitOfWork.Object);


			//Act
			await postService.CreatePost(post);
			var actual = (await postService.GetPosts()).FirstOrDefault();

			//Assert
			Assert.IsNotNull(actual);
			Assert.AreEqual(actual.Id, post.Id);
			Assert.AreEqual(actual.Content, post.Content);
			Assert.AreEqual(actual.Title, post.Title);
		}

		[TestMethod]
		[ExpectedException(typeof(ServiceException))]
		public async Task PostService_CreatePost_ThrowsExceptionIfPostIsNull()
		{
			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.Posts.GetAll()).Returns(GetTestPosts());
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestProfiles());
			var postService = new PostService(mockUnitOfWork.Object);
			
			await postService.CreatePost(null);
		}

		[TestMethod]
		[ExpectedException(typeof(ServiceException))]
		public async Task PostService_GetPost_ThrowsExceptionIfPostIsNull()
		{
			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.Posts.GetAll()).Returns(GetTestPosts());
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestProfiles());
			var postService = new PostService(mockUnitOfWork.Object);

			await postService.GetPost(1);
		}

		#region Utility
		public async Task<IEnumerable<Post>> GetTestPosts()
		{
			var posts = new List<Post>
			{
				new Post { Id = 1, ClientProfile = GetTestProfile(), Title = "Test", Content = "Content", CreatedAt = DateTime.Now, IsDeleted = false },
				new Post { Id = 2, ClientProfile = GetTestProfile(), Title = "Some title", Content = "Hey", CreatedAt = DateTime.Now, IsDeleted = false },
				new Post { Id = 3, ClientProfile = GetTestProfile(), Title = "Ttile", Content = "How are you", CreatedAt = DateTime.Now, IsDeleted = false },
			};
			return posts;
		}

		public ClientProfile GetTestProfile()
		{
			var profile = new ClientProfile
			{
				Id = "test1",
				Name = "Tom",
				BirthdayDate =
				DateTime.Now.AddYears(-20),
				IsDeleted = false
			};
			return profile;
		}

		public async Task<IEnumerable<ClientProfile>> GetTestProfiles()
		{
			var profiles = new List<ClientProfile>
			{
				new ClientProfile { Id = "testId", Name = "Tom", BirthdayDate = DateTime.Now.AddYears(-20), IsDeleted = false }
			};
			return profiles;
		}
		#endregion
	}
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Services;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.Tests.BLL
{
	[TestClass]
	public class MessageServiceTests
	{
		[TestMethod]
		public async Task MessageService_SendMessage_AddsMessageToDb()
		{
			//Arrange
			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.Messages.GetAll()).Returns(GetTestMessages());
			var userService = new MessageService(mockUnitOfWork.Object);
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestProfiles());
			MessageDTO message = new MessageDTO
			{
				Id = 1,
				SenderId = "testId",
				Content = "Test",
				ReceiverId = "testId2",
				IsDeleted = false,
				CreatedAt = DateTime.Now
			};

			//Act
			await userService.SendMessage(message);
			var actual = (await userService.GetUserMessages("testId")).FirstOrDefault();

			//Assert
			Assert.IsNotNull(actual);
			Assert.AreEqual(message.Content, actual.Content);
			Assert.AreEqual(message.SenderId, actual.SenderId);
			Assert.AreEqual(message.ReceiverId, actual.ReceiverId);
		}

		[TestMethod]
		[ExpectedException(typeof(ServiceException))]
		public async Task MessageService_DeleteMessage_ThrowsExceptionIfMessageDoesNotExists()
		{
			//Arrange
			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.Messages.GetAll()).Returns(GetTestMessages());
			var userService = new MessageService(mockUnitOfWork.Object);
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestProfiles());
			MessageDTO message = new MessageDTO
			{
				Id = 120,
				SenderId = "testId",
				Content = "Test",
				ReceiverId = "testId2",
				IsDeleted = false,
				CreatedAt = DateTime.Now
			};

			await userService.DeleteMessage(message);
		}

		[TestMethod]
		[ExpectedException(typeof(ServiceException))]
		public async Task MessageService_EditMessage_ThrowsExceptionIfMessageDoesNotExists()
		{
			//Arrange
			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.Messages.GetAll()).Returns(GetTestMessages());
			var userService = new MessageService(mockUnitOfWork.Object);
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestProfiles());
			MessageDTO message = new MessageDTO
			{
				Id = 120,
				SenderId = "testId",
				Content = "Test test",
				ReceiverId = "testId2",
				IsDeleted = false,
				CreatedAt = DateTime.Now
			};
			await userService.EditMessage(message);
		}

		#region Utility
		public async Task<IEnumerable<Message>> GetTestMessages()
		{
			var sender = new ClientProfile
			{
				Id = "testId",
				Name = "Tom",
				BirthdayDate = DateTime.Now.AddYears(-20),
				IsDeleted = false
			};

			var receiver = new ClientProfile
			{
				Id = "testId2", 
				Name = "Fred", 
				BirthdayDate = DateTime.Now.AddYears(-25), 
				IsDeleted = false
			};

			var messages = new List<Message>
			{
				new Message { Id = 1, ClientProfile = sender, Content = "Test", CreatedAt = DateTime.Now, IsDeleted = false, ReceiverId = receiver.Id, SenderId = sender.Id},
				new Message { Id = 2, ClientProfile = sender, Content = "Test2", CreatedAt = DateTime.Now, IsDeleted = false, ReceiverId = receiver.Id, SenderId = sender.Id},
				new Message { Id = 3, ClientProfile = receiver, Content = "Test2", CreatedAt = DateTime.Now, IsDeleted = false, ReceiverId = sender.Id, SenderId = receiver.Id}
			};
			return messages;
		}

		public async Task<IEnumerable<ClientProfile>> GetTestProfiles()
		{
			var profiles = new List<ClientProfile>
			{
				new ClientProfile { Id = "testId", Name = "Tom", BirthdayDate = DateTime.Now.AddYears(-20), IsDeleted = false }
			};
			return profiles;
		}
		#endregion
	}
}

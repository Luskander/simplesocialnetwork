﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.BLL.Services;
using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.Tests
{
	[TestClass]
	public class UserServiceTests
	{
		[TestMethod]
		public async Task UserService_GetUser_ReturnsCorrectUser()
		{
			//Arrange
			var user = new UserDTO
			{
				Id = "testId",
				Email = "testemail@gmail.com",
				Password = "test",
				Role = "User",
				Name = "Tom",
				IsDeleted = false,
			};

			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestClientProfiles());
			var userService = new UserService(mockUnitOfWork.Object);

			//Act
			var actual = await userService.GetUser(user.Id);

			//Assert
			Assert.IsNotNull(actual);
			Assert.AreEqual(actual.Id, user.Id);
		}

		[TestMethod]
		public async Task UserService_GetUsers_ReturnAllUsers()
		{
			//Arrange
			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestClientProfiles());
			var userService = new UserService(mockUnitOfWork.Object);
			var expected = await GetTestUserDTO();

			//Act
			var actual = await userService.GetUsers();

			//Assert
			Assert.IsNotNull(actual);
			Assert.AreEqual(expected.FirstOrDefault().Id, actual.FirstOrDefault().Id);
		}

		[TestMethod]
		[ExpectedException(typeof(ValidationException))]
		public async Task UserService_GetUser_ThrowsExceptionIfUserDoesNotExist()
		{
			var user = new UserDTO
			{
				Id = "qwerty",
				Email = "testemail@gmail.com",
				Password = "test",
				Role = "User",
				Name = "Tom",
				IsDeleted = false,
			};

			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestClientProfiles());
			var userService = new UserService(mockUnitOfWork.Object);

			var actual = await userService.GetUser(user.Id);
		}

		[TestMethod]
		[ExpectedException(typeof(ValidationException))]
		public async Task UserService_GetUser_ThrowsExceptionWhenIdIsEmpty()
		{
			var user = new UserDTO
			{
				Id = "",
				Email = "testemail@gmail.com",
				Password = "test",
				Role = "User",
				Name = "Tom",
				IsDeleted = false,
			};

			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestClientProfiles());
			var userService = new UserService(mockUnitOfWork.Object);

			var actual = await userService.GetUser(user.Id);
		}

		[TestMethod]
		public async Task UserService_UpdateUser_WorksCorrectly()
		{
			//Arrange
			var user = new UserDTO
			{
				Id = "testId",
				Role = "User",
				Name = "New name",
				About = "New about",
				IsDeleted = false,
			};

			var mockUnitOfWork = new Mock<IUnitOfWork>();
			mockUnitOfWork.Setup(x => x.ClientProfiles.GetAll()).Returns(GetTestClientProfiles());
			var userService = new UserService(mockUnitOfWork.Object);


			//Act
			await userService.UpdateUser(user);
			var actual = await userService.GetUser(user.Id);


			//Assert
			Assert.IsNotNull(actual);
			Assert.AreEqual(user.Id, user.Id);
			Assert.AreEqual(user.Name, actual.Name);
			Assert.AreEqual(user.About, actual.About);
		}

		#region Utility
		public async Task<IEnumerable<UserDTO>> GetTestUserDTO()
		{
			var users = new List<UserDTO>
			{
				new UserDTO { Id = "testId", Name = "Tom", BirthdayDate = DateTime.Now.AddYears(-20), IsDeleted = false},
				new UserDTO { Id = "testId2", Name = "Fred", BirthdayDate = DateTime.Now.AddYears(-25), IsDeleted = false},
				new UserDTO { Id = "testId3", Name = "Bill", BirthdayDate = DateTime.Now.AddYears(-15), IsDeleted = false},
			};
			return users;
		}

		public async Task<IEnumerable<ClientProfile>> GetTestClientProfiles()
		{
			var clientProfiles = new List<ClientProfile>
			{
				new ClientProfile { Id = "testId", Name = "Tom", BirthdayDate = DateTime.Now.AddYears(-20), IsDeleted = false},
				new ClientProfile { Id = "testId2", Name = "Fred", BirthdayDate = DateTime.Now.AddYears(-25), IsDeleted = false},
				new ClientProfile { Id = "testId3", Name = "Bill", BirthdayDate = DateTime.Now.AddYears(-15), IsDeleted = false},
			};
			return clientProfiles;
		}

		public ClientProfile GetTestProfile()
		{
			var profile = new ClientProfile 
			{
				Id = "test1", 
				Name = "Tom", 
				BirthdayDate = 
				DateTime.Now.AddYears(-20), 
				IsDeleted = false 
			};
			return profile;
		}
		#endregion
	}
}

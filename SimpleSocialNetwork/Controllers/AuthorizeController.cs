﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SimpleSocialNetwork.Controllers
{
    public class AuthorizeController : Controller
    {
        readonly IUserService userService;
		public AuthorizeController(IUserService service)
		{
            userService = service;
		}

        public ActionResult Index()
		{
            return RedirectToAction("Login", "Authorize");
		}

        [Route("Login")]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "User");
            }
            return View();
        }

        /// <summary>
        /// Action for login to user profile
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public async Task<ActionResult> Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                UserDTO user = new UserDTO() { Email = login.Email, Password = login.Password };
                ClaimsIdentity claim = await userService.Authenticate(user);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Invalid username or password");
                }
                else
                {
                    HttpContext.GetOwinContext().Authentication.SignOut();
                    HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "User");
                }
            }
            return View(login);
        }

        public ActionResult Logout()
		{
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Login");
		}

        [Route("Register")]
        public ActionResult Register()
		{
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "User");
            }
            return View();
		}

        /// <summary>
        /// Register new account in system
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Register")]
        public async Task<ActionResult> Register(ClientProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO user = new UserDTO
                {
                    Name = model.Name,
                    Email = model.Email,
                    Avatar = model.Avatar,
                    CreatedAt = DateTime.Now,
                    BirthdayDate = model.BirthdayDate,
                    Password = model.Password,
                    Role = "User",
                };

                OperationDetails operationDetails = await userService.CreateUser(user);
                if (operationDetails.Succeded)
                {
                    return RedirectToAction("RegisterComplete");
                }
                else
                {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }
            return View(model);
        }

        [Route("RegisterComplete")]
        public ActionResult RegisterComplete()
		{
            return View();
		}
    }
}
﻿using AutoMapper;
using PagedList;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.BLL.Services;
using SimpleSocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SimpleSocialNetwork.Controllers
{
	[Authorize]
	public class UserController : Controller
	{
		private readonly IUserService userService;
		private readonly IPostService postService;
		private readonly IFriendsRelationService friendService;
		private readonly IMessageService messageService;

		public UserController(IUserService user, IPostService post, IMessageService message, IFriendsRelationService friends)
		{
			userService = user;
			postService = post;
			messageService = message;
			friendService = friends;
		}

		#region User

		/// <summary>
		/// Action for render main page with certain partial view and page on it
		/// </summary>
		/// <param name="partial">Represents name of partial view to render</param>
		/// <param name="page">Page of partial view</param>
		/// <returns>Main page</returns>
		[Route("")]
		public async Task<ActionResult> Index(string partial, int? page)
		{
			if (User.IsInRole("Admin"))
			{
				return RedirectToAction("Index", "Admin");
			}

			try
			{
				var user = (await userService.GetUsers()).FirstOrDefault(x => x.Email.Equals(User.Identity.Name));
				if (user != null)
				{
					var userFriends = await friendService.GetUserFriends(user);
					ViewBag.Friends = (await GetUsers()).Where(x => userFriends.Any(item => x.Id == item.FriendId));

					var config = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, ClientProfileViewModel>());
					var mapper = new Mapper(config);
					ClientProfileViewModel profile = mapper.Map<UserDTO, ClientProfileViewModel>(user);
					
					int pageNumber = (page ?? 1);
					await RenderPartial(partial, pageNumber);
					return View(profile);
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("user", ex.Message);
			}
			return RedirectToAction("NotFound", "Error");
		}

		/// <summary>
		/// Action for render user profile page based on id
		/// </summary>
		/// <param name="id">Represents id of user page to show</param>
		/// <returns>User page profile</returns>
		[Route("UserProfile")]
		public async Task<ActionResult> UserProfile(string id)
		{
			var userId = await GetCurrentUserId();
			if (id == userId)
			{
				return RedirectToAction("Index", "User");
			}

			try
			{
				var user = await userService.GetUser(id);
				if(user != null)
				{
					var relations = await friendService.GetUserFriends(user);
					ViewBag.IsFriend = relations.Where(x => x.UserId.Equals(id))
						.Any(x => x.FriendId.Equals(userId));
					ViewBag.Posts = await GetUserPosts(id);

					var config = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, ClientProfileViewModel>());
					var mapper = new Mapper(config);
					ClientProfileViewModel profile = mapper.Map<UserDTO, ClientProfileViewModel>(user);
					return View(profile);
				}
			}
			catch(Exception ex)
			{
				ModelState.AddModelError("user", ex.Message);
			}
			return RedirectToAction("Index", "User");
		}

		/// <summary>
		/// Renders partial view based on parameters
		/// </summary>
		/// <param name="partial">Name of partial view</param>
		/// <param name="page">Page of partial view</param>
		/// <returns>Partial view</returns>
		[ChildActionOnly]
		public async Task<PartialViewResult> RenderPartial(string partial, int? page)
		{
			int pageNumber = (page ?? 1);
			if (partial == null || partial == "PostForm")
			{
				return await PostForm(pageNumber);
			}
			else if(partial == "Feed")
			{
				return await Feed(pageNumber);
			}
			else
			{
				throw new ArgumentException("No partial view with such name");
			}
		}
		#endregion

		#region Post

		/// <summary>
		/// Set up post form with page on it
		/// </summary>
		/// <param name="page">Page of posts</param>
		/// <returns>Partial view of posts</returns>
		[OutputCache(Duration = 360)]
		public async Task<PartialViewResult> PostForm(int? page)
		{
			int pageNumber = (page ?? 1);
			int pageSize = 15;
			var userId = await GetCurrentUserId();
			var posts = await GetUserPosts(userId);
			PaginationPostViewModel model = new PaginationPostViewModel
			{
				Posts = posts.ToPagedList(pageNumber, pageSize),
				NewPost = new PostViewModel()
			};
			ViewBag.Pagination = model;
			return PartialView(model);
		}

		/// <summary>
		/// Creates new post on partial view
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		public async Task<ActionResult> PostForm(PaginationPostViewModel model)
		{
			PostViewModel postModel = model.NewPost;
			postModel.OwnerId = await GetCurrentUserId();
			var config = new MapperConfiguration(cfg => cfg.CreateMap<PostViewModel, PostDTO>());
			var mapper = new Mapper(config);
			PostDTO post = mapper.Map<PostViewModel, PostDTO>(postModel);
			try
			{
				if(ModelState.IsValid)
				{
					await postService.CreatePost(post);
				}
			}
			catch(ServiceException ex)
			{
				ModelState.AddModelError("model", ex.Message);
			}
			await GetUserPosts(postModel.OwnerId);
			return RedirectToAction("Index", "User");
		}

		[Route("ChangePost")]
		public async Task<ActionResult> ChangePost(int id)
		{
			try
			{
				var post = await postService.GetPost(id);
				if (post != null && !post.IsDeleted)
				{
					PostViewModel model = new PostViewModel()
					{
						Content = post.Content,
						Id = post.Id,
						IsDeleted = post.IsDeleted,
						Title = post.Title,
					};
					return View(model);
				}
			}
			catch(ServiceException ex)
			{
				ModelState.AddModelError("post", ex.Message);
			}
			return RedirectToAction("Index", "User");
		}

		[HttpPost]
		[Route("ChangePost")]
		public async Task<ActionResult> ChangePost(PostViewModel model)
		{
			try
			{
				var post = await postService.GetPost(model.Id);
				if (post != null && !post.IsDeleted)
				{
					PostDTO postDto = new PostDTO()
					{
						Content = model.Content,
						Id = model.Id,
						IsDeleted = model.IsDeleted,
						Title = model.Title,
					};
					if(ModelState.IsValid)
					{
						await postService.EditPost(postDto);
					}
					return View(model);
				}
			}
			catch (ServiceException ex)
			{
				ModelState.AddModelError("post", ex.Message);
			}
			return RedirectToAction("Index", "User");
		}

		[HttpPost]
		[Route("DeletePost")]
		public async Task<ActionResult> DeletePost(int id)
		{
			try
			{
				var post = await postService.GetPost(id);
				if (post != null && !post.IsDeleted)
				{
					post.IsDeleted = true;
					await postService.EditPost(post);
					return RedirectToAction("Index", "User");
				}
			}
			catch(ServiceException ex)
			{
				Content(ex.Message);
			}
			return RedirectToAction("Index", "User");
		}

		/// <summary>
		/// Represents action for showing partial view with feed posts based on user friends
		/// </summary>
		/// <param name="page">Page of feed</param>
		/// <returns>Partial view of feed</returns>
		[OutputCache(Duration = 360)]
		public async Task<PartialViewResult> Feed(int? page)
		{
			try
			{
				List<UserPostViewModel> userPosts = new List<UserPostViewModel>();
				var userId = await GetCurrentUserId();
				UserDTO currentUser = await userService.GetUser(userId);
				var userFriends = await friendService.GetUserFriends(currentUser);

				foreach(var user in userFriends)
				{
					var profile = (await GetUsers())
						.FirstOrDefault(x => x.Id.Equals(user.FriendId));
					var posts = await GetUserPosts(user.FriendId);
					foreach(var post in posts)
					{
						UserPostViewModel newPost = new UserPostViewModel
						{
							Content = post.Content,
							CreatedAt = post.CreatedAt,
							Title = post.Title,
							Owner = profile
						};
						userPosts.Add(newPost);
					}
				}
				int pageNumber = (page ?? 1);
				int pageSize = 15;
				ViewBag.Pagination = userPosts.OrderBy(x => x.CreatedAt).ToPagedList(pageNumber, pageSize);
				return PartialView(ViewBag.Pagination);
			}
			catch(Exception ex)
			{
				Content(ex.Message);
			}
			return PartialView("PostForm");
		}
		#endregion

		#region Friends

		/// <summary>
		/// Set up user friends page
		/// </summary>
		/// <returns>View of friends</returns>
		[Route("Friends")]
		public async Task<ActionResult> Friends()
		{
			try
			{
				var userId = await GetCurrentUserId();
				UserDTO currentUser = await userService.GetUser(userId);
				var userFriends = await friendService.GetUserFriends(currentUser);
				ViewBag.Users = (await GetUsers()) 
					.Where(x => !userFriends.Any(item => x.Id == item.FriendId))
					.OrderByDescending(x => x.Name);
			}
			catch(Exception ex)
			{
				ModelState.AddModelError("friends", ex.Message);
			}
			return View();
		}

		/// <summary>
		/// Refresh friends page with users based on search criteria
		/// </summary>
		/// <param name="name">Represents search criteria for name or location of user</param>
		/// <returns>View of friends</returns>
		[HttpPost]
		[Route("Friends")]
		public async Task<ActionResult> Friends(string name)
		{
			try
			{
				var userId = await GetCurrentUserId();
				UserDTO currentUser = await userService.GetUser(userId);
				var userFriends = await friendService.GetUserFriends(currentUser);
				ViewBag.Users = (await GetUsers())
					.Where(x => !userFriends
					.Any(item => x.Id == item.FriendId))
					.Where(x => x.Name.Contains(name) || (x.Location != null && x.Location.Contains(name)))
					.OrderByDescending(x => x.Name);
			}
			catch(Exception ex)
			{
				ModelState.AddModelError("friends", ex.Message);
			}
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> AddFriend(string id)
		{
			try
			{
				var userId = await GetCurrentUserId();
				UserDTO currentUser = await userService.GetUser(userId);
				UserDTO friend = await userService.GetUser(id);
				await friendService.AddFriend(currentUser, friend);
			}
			catch(Exception ex)
			{
				ModelState.AddModelError("friend", ex.Message);
			}
			return View("Friends");
		}

		[HttpPost]
		public async Task<ActionResult> RemoveFriend(string id)
		{
			try
			{
				var userId = await GetCurrentUserId();
				UserDTO currentUser = await userService.GetUser(userId);
				UserDTO friend = await userService.GetUser(id);
				await friendService.RemoveFriend(currentUser, friend);
			}
			catch(Exception ex)
			{
				ModelState.AddModelError("friend", ex.Message);
			}
			return View("Friends");
		}
		#endregion

		#region Chat

		/// <summary>
		/// Set up chat with user based on id
		/// </summary>
		/// <param name="id">User id to chat with</param>
		/// <returns>Chat view</returns>
		[Route("Chat")]
		[OutputCache(Duration = 360)]
		public async Task<ActionResult> Chat(string id)
		{
			if (id == null)
			{
				Response.StatusCode = 404;
				return RedirectToAction("NotFound");
			}

			try
			{
				var userId = await GetCurrentUserId();
				ViewBag.FriendName = (await userService.GetUser(id)).Name;
				ViewBag.SenderId = userId;
				Session["FriendId"] = id;

				var userMessages = (await messageService.GetUserMessages(userId))
									.Where(x => x.ReceiverId.Equals(id));

				var friendMessages = (await messageService.GetUserMessages(id))
									.Where(x => x.ReceiverId.Equals(userId));

				var config = new MapperConfiguration(cfg => cfg.CreateMap<MessageDTO, MessageViewModel>());
				var mapper = new Mapper(config);
				var concat = userMessages.Concat(friendMessages);
				ViewBag.Messages = mapper.Map<List<MessageViewModel>>(concat).OrderBy(x => x.CreatedAt);
				return View("Chat");
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("messages", ex.Message);
			}

			return RedirectToAction("Chat", "User");
		}

		/// <summary>
		/// Sends new message to user
		/// </summary>
		/// <param name="model">Message to send</param>
		/// <returns></returns>
		[Route("Chat")]
		[HttpPost]
		public async Task<ActionResult> Chat(MessageViewModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var userId = await GetCurrentUserId();
					MessageDTO mes = new MessageDTO()
					{
						Content = model.Content,
						SenderId = userId,
						CreatedAt = DateTime.Now,
						IsDeleted = false,
						ReceiverId = Session["FriendId"].ToString()
					};
					await messageService.SendMessage(mes);
					return await Chat(mes.ReceiverId);
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("message", ex.Message);
				}
			}
			ModelState.AddModelError("message", "Enter text");
			return await Chat(Session["FriendId"].ToString());
		}
		#endregion

		#region Info

		/// <summary>
		/// Set up user form to change personal info
		/// </summary>
		/// <returns>User form view</returns>
		[Route("Info")]
		public async Task<ActionResult> Info()
		{
			try
			{
				var userId = await GetCurrentUserId();
				UserDTO user = await userService.GetUser(userId);
				if (user != null)
				{
					var config = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, ClientProfileViewModel>());
					var mapper = new Mapper(config);
					ClientProfileViewModel profile = mapper.Map<UserDTO, ClientProfileViewModel>(user);
					return View(profile);
				}
			}
			catch(ServiceException ex)
			{
				ModelState.AddModelError("user", ex.Message);
			}
			return RedirectToAction("Index", "User");
		}

		/// <summary>
		/// Updates user personal info
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("Info")]
		public async Task<ActionResult> Info(ClientProfileViewModel model)
		{
			var userId = await GetCurrentUserId();
			if (model.AvatarFile != null)
			{
				string fileName = System.IO.Path.GetFileName(model.AvatarFile.FileName);
				string directory = Server.MapPath($"~/Files/avatars/user-{userId}/");
				if (!System.IO.Directory.Exists(directory))
				{
					System.IO.Directory.CreateDirectory(directory);
				}
				model.AvatarFile.SaveAs(directory + fileName);
				model.Avatar = fileName;
			}

			var config = new MapperConfiguration(cfg => cfg.CreateMap<ClientProfileViewModel, UserDTO>());
			var mapper = new Mapper(config);
			UserDTO userDTO = mapper.Map<ClientProfileViewModel, UserDTO>(model);
			userDTO.Id = userId;

			try
			{
				if(ModelState.IsValid)
				{
					await userService.UpdateUser(userDTO);
				}
				else
				{
					ModelState.AddModelError("User", "Invalid data");
					throw new ValidationException("Model is invalid", "model");
				}	
			}
			catch (Exception ex)
			{
				Content(ex.Message);
			}
			return View(model);
		}
		#endregion

		#region Utility

		/// <summary>
		/// Utility method to get current user id
		/// </summary>
		/// <returns>String of current user id</returns>
		private async Task<string> GetCurrentUserId()
		{
			try
			{
				var users = await userService.GetUsers();
				var currentUser = users.FirstOrDefault(x => x.Email.Equals(User.Identity.Name));
				if (currentUser != null)
				{
					return currentUser.Id;
				}
			}
			catch(Exception ex)
			{
				Content(ex.Message);
			}
			
			return string.Empty;
		}

		/// <summary>
		/// Get users where email does not equal current user email
		/// </summary>
		/// <returns>List of ClientProfileViewModel</returns>
		private async Task<IEnumerable<ClientProfileViewModel>> GetUsers()
		{
			try
			{
				var config = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, ClientProfileViewModel>());
				var mapper = new Mapper(config);
				var users = (await userService.GetUsers()).Where(x => x.Email != User.Identity.Name).ToList();
				var map = mapper.Map<List<ClientProfileViewModel>>(users).OrderByDescending(x => x.Name);
				return map;
			}
			catch(Exception ex)
			{
				Content(ex.Message);
				return Enumerable.Empty<ClientProfileViewModel>();
			}
		}

		/// <summary>
		/// Get user posts based on id and set up ViewBag.Posts with it
		/// </summary>
		/// <param name="id">Represents user id</param>
		/// <returns>List of PostViewModel</returns>
		private async Task<IEnumerable<PostViewModel>> GetUserPosts(string id)
		{
			try
			{
				var user = (await userService.GetUsers()).FirstOrDefault(x => x.Id.Equals(id));
				var config = new MapperConfiguration(cfg => cfg.CreateMap<PostDTO, PostViewModel>());
				var mapper = new Mapper(config);
				var posts = (await postService.GetPosts())
					.Where(x => x.OwnerId.Equals(user.Id))
					.Where(x => x.IsDeleted.Equals(false))
					.ToList();
				var map = mapper.Map<List<PostViewModel>>(posts).OrderByDescending(x => x.CreatedAt);

				if (map != null)
				{
					return map;
				}
				else
				{
					PostViewModel postViewModel = new PostViewModel
					{
						Title = "Demo post",
						Content = "This is demo post. Try to write your own!",
						CreatedAt = DateTime.Now,
					};
					return new List<PostViewModel> { postViewModel };
				}
			}
			catch (Exception ex)
			{
				Content(ex.Message);
			}
			return Enumerable.Empty<PostViewModel>();
		}
		#endregion
	}
}
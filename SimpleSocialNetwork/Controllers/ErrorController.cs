﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleSocialNetwork.Controllers
{
    public class ErrorController : Controller
    {
        [Route("NotFound")]
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        [Route("AccessDenied")]
        public ActionResult AccessDenied()
		{
            Response.StatusCode = 403;
            return View();
		}

        [Route("BadRequest")]
        public ActionResult BadRequest()
        {
            Response.StatusCode = 400;
            return View();
        }

        [Route("Error")]
        public ActionResult Error()
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}
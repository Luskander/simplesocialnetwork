﻿using AutoMapper;
using PagedList;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SimpleSocialNetwork.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IUserService userService;
        private readonly IPostService postService;
        private readonly IMessageService messageService;
        public AdminController(IUserService user, IPostService post, IMessageService message)
        {
            userService = user;
            postService = post;
            messageService = message;
        }

        [Route("Admin")]
        public ActionResult Index()
        {
            return View();
        }

		#region Users

        /// <summary>
        /// Returns list of all users to view with search criteria and certain page
        /// </summary>
        /// <param name="searchString">Search criteria</param>
        /// <param name="page"></param>
        /// <returns></returns>
		public async Task<ActionResult> Users(string searchString, int? page)
        {
            var users = await GetUsers();
            if(!string.IsNullOrEmpty(searchString))
			{
                ViewBag.SearchString = searchString;
                users = users.Where(x => x.Id.Contains(searchString));
			}
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Set up form with user to edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> UserForm(string id)
        {
            if (id != null)
            {
                var userToEdit = (await GetUsers()).FirstOrDefault(x => x.Id.Equals(id));
                return View(userToEdit);
            }
            return RedirectToAction("Users", "Admin");
        }

        /// <summary>
        /// Update user with new info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UserForm(ClientProfileViewModel model)
        {
            if (model.AvatarFile != null)
            {
                string fileName = System.IO.Path.GetFileName(model.AvatarFile.FileName);
                string directory = Server.MapPath($"~/Files/avatars/user-{model.Id}/");
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                model.AvatarFile.SaveAs(directory + fileName);
                model.Avatar = fileName;
            }

            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<ClientProfileViewModel, UserDTO>());
                var mapper = new Mapper(config);
                UserDTO userDTO = mapper.Map<ClientProfileViewModel, UserDTO>(model);

                await userService.UpdateUser(userDTO);
            }
            catch (Exception ex)
            {
                Content(ex.Message);
            }
            ModelState.AddModelError("Model", "Model is invalid");
            return RedirectToAction("Users", "Admin");
        }

        public async Task<ActionResult> DeleteUser(string id)
        {
            try
            {
                var user = (await userService.GetUsers()).FirstOrDefault(x => x.Id.Equals(id));
                if (user != null)
                {
                    user.IsDeleted = true;
                    await userService.UpdateUser(user);
                    return RedirectToAction("Users", "Admin");
                }
            }
            catch(ServiceException ex)
			{
                Content(ex.Message);
			}
            return RedirectToAction("Index", "Admin");
        }

        public async Task<ActionResult> RestoreUser(string id)
        {
            try
			{
                var user = (await userService.GetUsers()).FirstOrDefault(x => x.Id.Equals(id));
                if (user != null)
                {
                    user.IsDeleted = false;
                    await userService.UpdateUser(user);
                    return RedirectToAction("Users", "Admin");
                }
            }
            catch(ServiceException ex)
			{
                Content(ex.Message);
            }
            return RedirectToAction("Index", "Admin");
        }
        #endregion

        #region Messages

        /// <summary>
        /// Returns list of all messages to view with search criteria and certain page
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<ActionResult> Messages(string searchString, int? page)
        {
            var messages = await GetMessages();
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;
                messages = messages.Where(x => x.SenderId.Contains(searchString));
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(messages.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Set up form with message to edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> MessageForm(int id)
        {
            var message = (await GetMessages()).FirstOrDefault(x => x.Id.Equals(id));
            if (message != null)
            {
                return View(message);
            }
            return RedirectToAction("Messages", "Admin");
        }

        /// <summary>
        /// Update message with new info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> MessageForm(MessageViewModel model)
        {
            MessageDTO message = new MessageDTO()
            {
                Content = model.Content,
                Id = model.Id,
                IsDeleted = model.IsDeleted
            };

            try
			{
                await messageService.EditMessage(message);
            }
            catch(ServiceException ex)
			{
                ModelState.AddModelError("message", ex.Message);
			}
            return RedirectToAction("Messages", "Admin");
        }

        public async Task<ActionResult> DeleteMessage(int id)
        {
            try
			{
                var message = (await messageService.GetMessages()).FirstOrDefault(x => x.Id.Equals(id));
                if (message != null)
                {
                    message.IsDeleted = true;
                    await messageService.EditMessage(message);
                    return RedirectToAction("Messages", "Admin");
                }
            }
            catch (ServiceException ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("Index", "Admin");
        }

        public async Task<ActionResult> RestoreMessage(int id)
        {
            try
            {
                var message = (await messageService.GetMessages()).FirstOrDefault(x => x.Id.Equals(id));
                if (message != null)
                {
                    message.IsDeleted = false;
                    await messageService.EditMessage(message);
                    return RedirectToAction("Messages", "Admin");
                }
            }
            catch (ServiceException ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("Index", "Admin");
        }
        #endregion

        #region Posts

        /// <summary>
        /// Returns list of all posts to view with search criteria and certain page
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<ActionResult> Posts(string searchString, int? page)
        {
            var posts = await GetPosts();
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;
                posts = posts.Where(x => x.OwnerId.Contains(searchString));
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(posts.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Set up form for post to edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> PostForm(int id)
		{
            var post = (await GetPosts()).FirstOrDefault(x => x.Id.Equals(id));
            if (post != null)
            {
                return View(post);
            }
            return RedirectToAction("Posts", "Admin");
        }

        /// <summary>
        /// Update post with new info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> PostForm(PostViewModel model)
        {
            PostDTO post = new PostDTO()
            {
                Title = model.Title,
                Content = model.Content,
                Id = model.Id,
                IsDeleted = model.IsDeleted
            };

            try
			{
               await postService.EditPost(post);
            }
            catch(ServiceException ex)
			{
                Content(ex.Message);
			}
            return RedirectToAction("Posts", "Admin");
        }

        public async Task<ActionResult> DeletePost(int id)
        {
            try
			{
                var post = (await postService.GetPosts()).FirstOrDefault(x => x.Id.Equals(id));
                if (post != null)
                {
                    post.IsDeleted = true;
                    await postService.EditPost(post);
                    return RedirectToAction("Posts", "Admin");
                }
            }
            catch (ServiceException ex)
            {
                Content(ex.Message);
            }
            return RedirectToAction("Index", "Admin");
        }

        public async Task<ActionResult> RestorePost(int id)
        {
            try
            {
                var post = (await postService.GetPosts()).FirstOrDefault(x => x.Id.Equals(id));
                if (post != null)
                {
                    post.IsDeleted = false;
                    await postService .EditPost(post);
                    return RedirectToAction("Posts", "Admin");
                }
            }
            catch (ServiceException ex)
            {
                Content(ex.Message);
            }
            return RedirectToAction("Index", "Admin");
        }
        #endregion

        #region Utility

        /// <summary>
        /// Get all users from database where email does not equal current user email
        /// </summary>
        /// <returns>List of ClientProfileViewModels</returns>
        private async Task<IEnumerable<ClientProfileViewModel>> GetUsers()
        {
            try
			{
                var config = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, ClientProfileViewModel>());
                var mapper = new Mapper(config);
                var users = (await userService.GetUsers()).Where(x => x.Email != User.Identity.Name).ToList();
                var map = mapper.Map<List<ClientProfileViewModel>>(users);
                return map;
            }
            catch(Exception ex)
			{
                Content(ex.Message);
                return Enumerable.Empty<ClientProfileViewModel>();
			}
        }

        /// <summary>
        /// Get all messages from database
        /// </summary>
        /// <returns>List of MessageViewModel</returns>
        private async Task<IEnumerable<MessageViewModel>> GetMessages()
        {
            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<MessageDTO, MessageViewModel>());
                var mapper = new Mapper(config);
                var messages = await messageService.GetMessages();
                var map = mapper.Map<List<MessageViewModel>>(messages);
                return map;
            }
            catch (Exception ex)
            {
                Content(ex.Message);
                return Enumerable.Empty<MessageViewModel>();
            }
        }

        /// <summary>
        /// Get all posts from database
        /// </summary>
        /// <returns>List of PostViewModel</returns>
        private async Task<IEnumerable<PostViewModel>> GetPosts()
        {
            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<PostDTO, PostViewModel>());
                var mapper = new Mapper(config);
                var posts = await postService.GetPosts();
                var map = mapper.Map<List<PostViewModel>>(posts);
                return map;
            }
            catch (Exception ex)
            {
                Content(ex.Message);
                return Enumerable.Empty<PostViewModel>();
            }
        }
		#endregion
	}
}
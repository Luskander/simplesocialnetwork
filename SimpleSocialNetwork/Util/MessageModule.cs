﻿using Ninject.Modules;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSocialNetwork.Util
{
	public class MessageModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IMessageService>().To<MessageService>();
		}
	}
}
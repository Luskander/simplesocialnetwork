﻿using Owin;
using Microsoft.Owin;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.BLL.Services;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace SimpleSocialNetwork.App_Start
{
	public class Startup
	{
		IServiceCreator serviceCreator = new ServiceCreator();
		public void Configuration(IAppBuilder app)
		{
			app.CreatePerOwinContext(CreateUserService);
			app.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				LoginPath = new PathString("/Login")
			});
		}

		private IUserService CreateUserService()
		{
			return serviceCreator.CreateUserService("DefaultConnection");
		}
	}
}
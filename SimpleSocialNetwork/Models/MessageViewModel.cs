﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleSocialNetwork.Models
{
	public class MessageViewModel
	{
		public int Id { get; set; }
		[Required(ErrorMessage = "Enter message")]
		[StringLength(300, MinimumLength = 1, ErrorMessage = "Message length must be from 1 to 300 symbols")]
		public string Content { get; set; }
		public string SenderId { get; set; }
		public string ReceiverId { get; set; }
		public DateTime CreatedAt { get; set; }
		public bool IsDeleted { get; set; }
	}
}
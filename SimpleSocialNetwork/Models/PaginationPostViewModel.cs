﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSocialNetwork.Models
{
	public class PaginationPostViewModel
	{
		public PagedList.IPagedList<PostViewModel> Posts { get; set; }
		public PostViewModel NewPost { get; set; }
	}
}
﻿using SimpleSocialNetwork.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SimpleSocialNetwork.Models
{
	public class ClientProfileViewModel
	{
		public string Id { get; set; }

		[Required(ErrorMessage = "Please enter name"), MaxLength(20)]
		[DataType(DataType.Text)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Enter email")]
		[EmailAddress(ErrorMessage = "Invalid Email Address")]
		public string Email { get; set; }

		[DataType(DataType.Password, ErrorMessage = "Enter valid password")]
		[StringLength(20, MinimumLength = 6, ErrorMessage = "Length must be between 6 and 20 symbols")]
		public string Password { get; set; }
		[StringLength(20, MinimumLength = 0, ErrorMessage = "Length must be below 20 symbols")]
		public string Status { get; set; }
		public string Avatar { get; set; }

		[StringLength(50, MinimumLength = 0, ErrorMessage = "Length must be below 50 symbols")]
		public string Location { get; set; }

		[StringLength(300, MinimumLength = 0, ErrorMessage = "Length must be below 300 symbols")]
		public string About { get; set; }
		public DateTime CreatedAt { get; set; }

		[Required(ErrorMessage = "Enter birthday date")]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
		[DataType(DataType.Date, ErrorMessage = "Invalid date. Input in format dd.mm.yyyy")]
		[Range(typeof(DateTime), "1/1/1900", "1/1/2009", ErrorMessage = "Range must be from 01.01.1900 to 01.01.2015")]
		public DateTime BirthdayDate { get; set; }
		public bool IsDeleted { get; set; }
		public HttpPostedFileBase AvatarFile { get; set; }

		public virtual ICollection<PostDTO> Posts { get; set; }
		public virtual ICollection<MessageDTO> Messages { get; set; }
	}
}
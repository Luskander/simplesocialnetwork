﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleSocialNetwork.Models
{
	public class LoginViewModel
	{
		public int ID { get; set; }
		[Required(ErrorMessage = "Enter email"), MaxLength(30)]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email")]
		public string Email { get; set; }
		[Required(ErrorMessage = "Enter password"), MaxLength(30)]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		public string Password { get; set; }
	}
}
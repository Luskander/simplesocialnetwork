﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleSocialNetwork.Models
{
	public class PostViewModel
	{
		public int Id { get; set; }
		public string OwnerId { get; set; }

		[Required(ErrorMessage = "Enter title")]
		[StringLength(20, MinimumLength = 1, ErrorMessage = "Title length must be from 1 to 20 symbols")]
		public string Title { get; set; }

		[Required(ErrorMessage = "Enter content")]
		[StringLength(500, MinimumLength = 1, ErrorMessage = "Title length must be from 1 to 500 symbols")]
		public string Content { get; set; }
		public DateTime CreatedAt { get; set; }
		public bool IsDeleted { get; set; }
	}
}
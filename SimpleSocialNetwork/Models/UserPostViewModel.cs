﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSocialNetwork.Models
{
	public class UserPostViewModel
	{
		public string Title { get; set; }
		public string Content { get; set; }
		public DateTime CreatedAt { get; set; }
		public ClientProfileViewModel Owner { get; set; }
	}
}
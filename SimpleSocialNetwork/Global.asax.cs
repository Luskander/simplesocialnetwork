using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SimpleSocialNetwork
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			Database.SetInitializer(new NetworkDbInitializer());

			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			NinjectModule userModule = new UserModule();
			NinjectModule postModule = new PostModule();
			NinjectModule messageModule = new MessageModule();
			NinjectModule friendsModule = new FriendsModule();
			NinjectModule serviceModule = new ServiceModule("DefaultConnection");
			var kernel = new StandardKernel(userModule, postModule, messageModule, serviceModule, friendsModule);
			kernel.Unbind<ModelValidatorProvider>();
			DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
		}
	}
}

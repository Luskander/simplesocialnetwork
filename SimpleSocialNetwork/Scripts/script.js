function addFriend(id) {
    $.ajax({
        url: '/User/AddFriend',
        data: { id: id },
        type: "POST",
        success: function () {
            alert('User added!');
        }
    });
    document.getElementById(id).textContent = "Added";
    document.getElementById(id).disabled = true;
}

function removeFriend(id) {
    $.ajax({
        url: '/User/RemoveFriend',
        data: { id: id },
        type: "POST",
        success: function () {
            alert('User removed!');
        }
    });
    document.getElementById(id).textContent = "Removed";
    document.getElementById(id).disabled = true;
}

function returnUserProfile(id) {
    $.ajax({
        url: '/UserProfile',
        type: "POST",
        data: { id: id },
        success: function () {
        }
    });
}

function startChat(id) {
    $.ajax({
        url: '/Chat' + id,
        type: "POST",
        data: { id: id },
        success: function () {
        }
    });
}

function sendMessage() {
    let message = document.getElementById("message").value;
    $.ajax({
        url: '/Chat' + id,
        type: "POST",
        data: { message: message },
        success: function () {
            document.getElementById("message").value = "";
        }
    });
}

function getPartial(id) {
    $.ajax({
        url: "/User/" + id,
        type: "GET",
        data: { id: id },
        success: function (data) {
            $('#placeHolderDiv').html(data);
        }
    });
}

function updatePost(id) {
    document.location = '/ChangePost?id=' + id;
}

function deletePost(id) {
    $.ajax({
        url: '/DeletePost',
        type: "POST",
        data: { id: id },
        success: function () {
            let btn = document.getElementById(id);
            btn.parentElement.parentElement.remove();
        }
    });
}

window.onload = function updateScroll() {
    let element = document.getElementById("chat");
    if (element) {
        element.scrollTop = element.scrollHeight;
    }
    let message = document.getElementById("message");
    if (message) {
        message.value = "";
    }
}
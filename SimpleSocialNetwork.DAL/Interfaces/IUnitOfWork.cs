﻿using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{
		IRepository<ClientProfile> ClientProfiles { get; }
		IRepository<Post> Posts { get; }
		IRepository<Message> Messages { get; }
		IRepository<FriendsRelations> FriendsRelations { get; }
		AppUserManager UserManager { get; }
		AppRoleManager RoleManager { get; }
		void Save();
		Task SaveAsync();
	}
}

﻿using SimpleSocialNetwork.DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Interfaces
{
	public interface IUnitOfWorkIdentity : IDisposable
	{
		ApplicationUserManager UserManager { get; }
		IUserProfileManager UserProfileManager { get; }
		ApplicationRoleManager RoleManager { get; }
		Task SaveAsync();
	}
}

﻿using SimpleSocialNetwork.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Interfaces
{
	public interface IUserProfileManager : IDisposable
	{
		void Create(User item);
	}
}

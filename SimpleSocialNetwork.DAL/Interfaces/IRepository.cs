﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Interfaces
{
	public interface IRepository<T> where T : class
	{
		Task<IEnumerable<T>> GetAll();
		Task<T> Get(int id);
		Task<IEnumerable<T>> Find(Func<T, bool> predicate);
		Task Create(T item);
		Task Update(T item);
		Task Delete(int id);
	}
}

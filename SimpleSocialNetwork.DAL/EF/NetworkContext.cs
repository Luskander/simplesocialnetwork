﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.UserIdentity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.EF
{
	public class NetworkContext : IdentityDbContext<AppUser>
	{
		public DbSet<ClientProfile> ClientProfiles { get; set; }
		public DbSet<Post> Posts { get; set; }
		public DbSet<Message> Messages { get; set; }
		public DbSet<FriendsRelations> FriendsRelations { get; set; }

		static NetworkContext()
		{
			Database.SetInitializer(new NetworkDbInitializer());
		}

		public NetworkContext() : base()
		{

		}

		public NetworkContext(string connectionString) 
			: base(connectionString)
		{
			
		}
	}

	public class NetworkDbInitializer : CreateDatabaseIfNotExists<NetworkContext>
	{
		protected override void Seed(NetworkContext context)
		{
			var userManager = new AppUserManager(new UserStore<AppUser>(context));

			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

			var role1 = new AppRole { Name = "Admin" };
			var role2 = new AppRole { Name = "User" };

			roleManager.Create(role1);
			roleManager.Create(role2);

			var admin = new AppUser { Email = "admin@gmail.com", UserName = "admin@gmail.com" };
			string password = "qwertY123";
			var result = userManager.Create(admin, password);

			if(result.Succeeded)
			{
				userManager.AddToRole(admin.Id, role1.Name);
				userManager.AddToRole(admin.Id, role2.Name);
			}

			if(!context.Posts.Any())
			{
				context.Posts.Add(new Post
				{
					Id = 1,
					Content = "This is random post to test functionality",
					CreatedAt = DateTime.Now,
					Title = "Great title",
					IsDeleted = false,
				});

				context.Posts.Add(new Post
				{
					Id = 2,
					Content = "yo is this",
					CreatedAt = DateTime.Now,
					Title = "Awesome title",
					IsDeleted = false,
				});
				context.SaveChanges();
			}
			base.Seed(context);
		}
	}
}

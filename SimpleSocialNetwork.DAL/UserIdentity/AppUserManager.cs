﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.UserIdentity
{
	public class AppUserManager : UserManager<AppUser>
	{
		public AppUserManager(IUserStore<AppUser> store)
			: base(store)
		{

		}

		public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
		{
			var manager = new AppUserManager(
			new UserStore<AppUser>(context.Get<NetworkContext>()));

			return manager;
		}
	}
}

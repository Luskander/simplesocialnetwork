﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSocialNetwork.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.UserIdentity
{
	public class AppRoleManager : RoleManager<AppRole>
	{
		public AppRoleManager(RoleStore<AppRole> store)
			: base(store)
		{

		}
	}
}

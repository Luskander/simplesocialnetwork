﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Entities
{
	[Table("AspNetUsers")]
	public class AppUser : IdentityUser
	{
		public virtual ClientProfile ClientProfile { get; set; }
	}
}

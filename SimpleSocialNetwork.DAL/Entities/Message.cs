﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Entities
{
	public class Message
	{
		public int Id { get; set; }
		public string Content { get; set; }
		public string SenderId { get; set; }
		public string ReceiverId { get; set; }
		public DateTime CreatedAt { get; set; }
		public bool IsDeleted { get; set; }

		public virtual ClientProfile ClientProfile { get; set; }
	}
}

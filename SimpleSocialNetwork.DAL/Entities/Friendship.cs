﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Entities
{
	[Table("FriendsRelations")]
	public class FriendsRelations
	{
		public int Id { get; set; }
		public string UserId { get; set; }
		public string FriendId { get; set; }
	}
}

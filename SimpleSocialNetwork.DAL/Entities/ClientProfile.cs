﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Entities
{
	[Table("ClientProfiles")]
	public class ClientProfile
	{
		[Key]
		[ForeignKey("AppUser")]
		public string Id { get; set; }
		public string Name { get; set; }
		public string? Status { get; set; }
		public string? Avatar { get; set; }
		public string? Location { get; set; }
		public string? About { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime BirthdayDate { get; set; }
		public bool IsDeleted { get; set; }

		public virtual AppUser AppUser { get; set; }
		public virtual ICollection<Post>? Posts { get; set; }
		public virtual ICollection<Message>? Messages { get; set; }
	}
}

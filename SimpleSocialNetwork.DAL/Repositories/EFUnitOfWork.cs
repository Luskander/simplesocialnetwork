﻿using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using SimpleSocialNetwork.DAL.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class EFUnitOfWork : IUnitOfWork
	{
		private readonly NetworkContext db;

		private UserRepository userRepository;
		private PostRepository postRepository;
		private MessageRepository messageRepository;
		private FriendsRepository friendsRepository;

		private readonly AppUserManager userManager;
		private readonly AppRoleManager roleManager;

		public EFUnitOfWork(string connectionString)
		{
			db = new NetworkContext(connectionString);
			userManager = new AppUserManager(new UserStore<AppUser>(db));
			roleManager = new AppRoleManager(new RoleStore<AppRole>(db));
		}

		public IRepository<ClientProfile> ClientProfiles
		{
			get
			{
				if(userRepository == null)
				{
					userRepository = new UserRepository(db);
				}
				return userRepository;
			}
		}

		public IRepository<Post> Posts
		{
			get
			{
				if(postRepository == null)
				{
					postRepository = new PostRepository(db);
				}
				return postRepository;
			}
		}

		public IRepository<Message> Messages
		{
			get
			{
				if(messageRepository == null)
				{
					messageRepository = new MessageRepository(db);
				}
				return messageRepository;
			}
		}

		public IRepository<FriendsRelations> FriendsRelations
		{
			get
			{
				if (friendsRepository == null)
				{
					friendsRepository = new FriendsRepository(db);
				}
				return friendsRepository;
			}
		}

		public AppUserManager UserManager
		{
			get { return userManager; }
		}

		public AppRoleManager RoleManager
		{
			get { return roleManager; }
		}

		public void Save()
		{
			db.SaveChanges();
		}

		public async Task SaveAsync()
		{
			await db.SaveChangesAsync();
		}

		private bool disposed = false;

		public void Dispose(bool disposing)
		{
			if(!disposed)
			{
				if(disposing)
				{
					db.Dispose();
				}
				disposed = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}

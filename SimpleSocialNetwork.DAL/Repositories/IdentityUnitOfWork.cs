﻿using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Identity;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class IdentityUnitOfWork : IUnitOfWorkIdentity
	{
		private readonly NetworkContext db;

		private readonly ApplicationUserManager userManager;
		private readonly ApplicationRoleManager roleManager;
		private readonly IUserProfileManager userProfileManager;

		public IdentityUnitOfWork(string connectionString)
		{
			db = new NetworkContext(connectionString);
			userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
			roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
			userProfileManager = new UserProfileManager(db);
		}

		public ApplicationUserManager UserManager
		{
			get { return userManager; }
		}

		public ApplicationRoleManager RoleManager
		{
			get { return roleManager; }
		}

		public IUserProfileManager UserProfileManager
		{
			get { return userProfileManager; }
		}

		public async Task SaveAsync()
		{
			await db.SaveChangesAsync();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		private bool disposed = false;

		public virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					userManager.Dispose();
					roleManager.Dispose();
					userProfileManager.Dispose();
				}
				disposed = true;
			}
		}
	}
}

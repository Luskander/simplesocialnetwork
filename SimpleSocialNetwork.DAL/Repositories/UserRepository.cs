﻿using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class UserRepository : IRepository<ClientProfile>
	{
		private readonly NetworkContext db;
		public UserRepository(NetworkContext context)
		{
			db = context;
		}
		public async Task Create(ClientProfile item)
		{
			db.ClientProfiles.Add(item);
		}

		public async Task Delete(int id)
		{
			var user = await db.ClientProfiles.FindAsync(id);
			if (user != null && !user.IsDeleted)
			{
				user.IsDeleted = true;
			}
		}

		public async Task Update(ClientProfile item)
		{
			db.Entry(item).State = EntityState.Modified;
		}

		public async Task<IEnumerable<ClientProfile>> Find(Func<ClientProfile, bool> predicate)
		{
			return db.ClientProfiles.Where(predicate);
		}

		public Task<ClientProfile> Get(int id)
		{
			throw new NotImplementedException();
		}

		public async Task<IEnumerable<ClientProfile>> GetAll()
		{
			return await db.ClientProfiles.ToListAsync();
		}
	}
}

﻿using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class UserProfileManager : IUserProfileManager, IRepository<User>
	{
		public NetworkContext Database { get; set; }
		public UserProfileManager(NetworkContext db)
		{
			Database = db;
		}
		public void Create(User item)
		{
			Database.UserProfiles.Add(item);
			Database.SaveChanges();
		}

		public IEnumerable<User> GetAll()
		{
			return Database.UserProfiles;
		}

		public User Get(int id)
		{
			return Database.UserProfiles.Find(id);
		}

		public IEnumerable<User> Find(Func<User, bool> predicate)
		{
			return Database.UserProfiles.Where(predicate).ToList();
		}

		public void Update(User item)
		{
			Database.Entry(item).State = System.Data.Entity.EntityState.Modified;
		}

		public void Delete(int id)
		{
			var user = Database.UserProfiles.Find(id);
			if (user != null && !user.IsDeleted)
			{
				user.IsDeleted = true;
			}
		}

		public void Dispose()
		{
			Database.Dispose();
		}
	}
}

﻿using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class PostRepository : IRepository<Post>
	{
		private readonly NetworkContext db;
		public PostRepository(NetworkContext context)
		{
			db = context;
		}
		public async Task Create(Post item)
		{
			db.Posts.Add(item);
		}

		public async Task Delete(int id)
		{
			var post = await db.Posts.FindAsync(id);
			if (post != null && !post.IsDeleted)
			{
				post.IsDeleted = true;
			}
		}

		public async Task Update(Post item)
		{
			db.Entry(item).State = EntityState.Modified;
		}

		public async Task<IEnumerable<Post>> Find(Func<Post, bool> predicate)
		{
			return db.Posts.Where(predicate);
		}

		public async Task<Post> Get(int id)
		{
			return await db.Posts.FindAsync(id);
		}

		public async Task<IEnumerable<Post>> GetAll()
		{
			return await db.Posts.ToListAsync();
		}
	}
}

﻿using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class MessageRepository : IRepository<Message>
	{
		private readonly NetworkContext db;
		public MessageRepository(NetworkContext context)
		{
			db = context;
		}
		public async Task Create(Message item)
		{
			db.Messages.Add(item);
		}

		public async Task Delete(int id)
		{
			var message = await db.Messages.FindAsync(id);
			if (message != null && !message.IsDeleted)
			{
				message.IsDeleted = true;
			}
		}

		public async Task Update(Message item)
		{
			db.Entry(item).State = EntityState.Modified;
		}

		public async Task<IEnumerable<Message>> Find(Func<Message, bool> predicate)
		{
			return db.Messages.Where(predicate);
		}

		public async Task<Message> Get(int id)
		{
			return await db.Messages.FindAsync(id);
		}

		public async Task<IEnumerable<Message>> GetAll()
		{
			return await db.Messages.ToListAsync();
		}
	}
}

﻿using SimpleSocialNetwork.DAL.EF;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.DAL.Repositories
{
	public class FriendsRepository : IRepository<FriendsRelations>
	{
		private readonly NetworkContext db;
		public FriendsRepository(NetworkContext context)
		{
			db = context;
		}
		public async Task Create(FriendsRelations item)
		{
			db.FriendsRelations.Add(item);
		}

		public async Task Delete(int id)
		{
			var relation = await db.FriendsRelations.FindAsync(id);
			db.FriendsRelations.Remove(relation);
		}

		public async Task Update(FriendsRelations item)
		{
			db.Entry(item).State = EntityState.Modified;
		}

		public async Task<IEnumerable<FriendsRelations>> Find(Func<FriendsRelations, bool> predicate)
		{
			return db.FriendsRelations.Where(predicate);
		}

		public async Task<FriendsRelations> Get(int id)
		{
			return await db.FriendsRelations.FindAsync(id);
		}

		public async Task<IEnumerable<FriendsRelations>> GetAll()
		{
			return await db.FriendsRelations.ToListAsync();
		}
	}
}

﻿namespace SimpleSocialNetwork.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialNetworkDBv6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientProfiles", "Location", c => c.String());
            AddColumn("dbo.ClientProfiles", "About", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClientProfiles", "About");
            DropColumn("dbo.ClientProfiles", "Location");
        }
    }
}

﻿namespace SimpleSocialNetwork.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialNetworkDBv3 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Posts", "OwnderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "OwnerId", c => c.String());
        }
    }
}

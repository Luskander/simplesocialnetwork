﻿namespace SimpleSocialNetwork.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialNetworkDBv4 : DbMigration
    {
        public override void Up()
        {
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FriendsRelations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FriendId = c.String(),
                        ClientId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}

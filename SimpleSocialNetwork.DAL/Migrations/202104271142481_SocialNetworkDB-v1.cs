﻿namespace SimpleSocialNetwork.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialNetworkDBv1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Messages", "Receiver_Id", "dbo.Users");
            DropForeignKey("dbo.Messages", "Sender_Id", "dbo.Users");
            DropForeignKey("dbo.Posts", "Owner_Id", "dbo.Users");
            DropIndex("dbo.Messages", new[] { "Receiver_Id" });
            DropIndex("dbo.Messages", new[] { "Sender_Id" });
            DropIndex("dbo.Posts", new[] { "Owner_Id" });
            CreateTable(
                "dbo.ClientProfiles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Status = c.String(),
                        Avatar = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        BirthdayDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .Index(t => t.Id);
            
            AddColumn("dbo.Messages", "ReceiverId", c => c.String());
            AddColumn("dbo.Messages", "ClientProfile_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Posts", "OwnderId", c => c.String());
            AddColumn("dbo.Posts", "ClientProfile_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Messages", "ClientProfile_Id");
            CreateIndex("dbo.Posts", "ClientProfile_Id");
            AddForeignKey("dbo.Messages", "ClientProfile_Id", "dbo.ClientProfiles", "Id");
            AddForeignKey("dbo.Posts", "ClientProfile_Id", "dbo.ClientProfiles", "Id");
            DropColumn("dbo.Messages", "Receiver_Id");
            DropColumn("dbo.Messages", "Sender_Id");
            DropColumn("dbo.Posts", "Owner_Id");
            DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                        Status = c.String(),
                        Avatar = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        BirthdayDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "BirthdayDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "CreatedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Avatar", c => c.String());
            AddColumn("dbo.AspNetUsers", "Status", c => c.String());
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.Posts", "Owner_Id", c => c.Int());
            AddColumn("dbo.Messages", "Sender_Id", c => c.Int());
            AddColumn("dbo.Messages", "Receiver_Id", c => c.Int());
            DropForeignKey("dbo.Posts", "ClientProfile_Id", "dbo.ClientProfiles");
            DropForeignKey("dbo.Messages", "ClientProfile_Id", "dbo.ClientProfiles");
            DropForeignKey("dbo.ClientProfiles", "Id", "dbo.AspNetUsers");
            DropIndex("dbo.Posts", new[] { "ClientProfile_Id" });
            DropIndex("dbo.Messages", new[] { "ClientProfile_Id" });
            DropIndex("dbo.ClientProfiles", new[] { "Id" });
            DropColumn("dbo.Posts", "ClientProfile_Id");
            DropColumn("dbo.Posts", "OwnderId");
            DropColumn("dbo.Messages", "ClientProfile_Id");
            DropColumn("dbo.Messages", "ReceiverId");
            DropTable("dbo.ClientProfiles");
            CreateIndex("dbo.Posts", "Owner_Id");
            CreateIndex("dbo.Messages", "Sender_Id");
            CreateIndex("dbo.Messages", "Receiver_Id");
            AddForeignKey("dbo.Posts", "Owner_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Messages", "Sender_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Messages", "Receiver_Id", "dbo.Users", "Id");
        }
    }
}

﻿using SimpleSocialNetwork.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Interfaces
{
	public interface IMessageService
	{
		Task SendMessage(MessageDTO message);
		Task EditMessage(MessageDTO message);
		Task DeleteMessage(MessageDTO message);
		Task<IEnumerable<MessageDTO>> GetUserMessages(string userId);
		Task<IEnumerable<MessageDTO>> GetMessages();
		void Dispose();
	}
}

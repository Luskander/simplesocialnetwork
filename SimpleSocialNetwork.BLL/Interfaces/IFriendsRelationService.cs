﻿using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Interfaces
{
	public interface IFriendsRelationService
	{
		Task<OperationDetails> AddFriend(UserDTO profile, UserDTO friend);
		Task<OperationDetails> RemoveFriend(UserDTO profile, UserDTO friend);
		Task<IEnumerable<FriendshipDTO>> GetUserFriends(UserDTO user);
	}
}

﻿using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Interfaces
{
	public interface IUserService
	{
		Task<OperationDetails> CreateUser(UserDTO user);
		Task<ClaimsIdentity> Authenticate(UserDTO user);
		Task<UserDTO> GetUser(string id);
		Task UpdateUser(UserDTO user);
		Task<IEnumerable<UserDTO>> GetUsers();
		void Dispose();
	}
}

﻿using SimpleSocialNetwork.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Interfaces
{
	public interface IPostService
	{
		Task CreatePost(PostDTO postDto);
		Task EditPost(PostDTO postDto);
		Task<PostDTO> GetPost(int id);
		Task<IEnumerable<PostDTO>> GetPosts();
		void Dispose();
	}
}

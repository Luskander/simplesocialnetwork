﻿using AutoMapper;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Services
{
	public class PostService : IPostService
	{
		IUnitOfWork Database { get; set; }

		public PostService(IUnitOfWork uow)
		{
			Database = uow;
		}
		public async Task CreatePost(PostDTO postDto)
		{
			if(postDto != null)
			{
				Post post = new Post()
				{
					Content = postDto.Content,
					Title = postDto.Title,
					CreatedAt = DateTime.Now,
					ClientProfile = (await Database.ClientProfiles.Find(x => x.Id.Equals(postDto.OwnerId))).FirstOrDefault(),
					IsDeleted = false,
				};
				await Database.Posts.Create(post);
				await Database.SaveAsync();
			}
			else
			{
				throw new ServiceException("Post is null", "postDto");
			}
		}

		public async Task<PostDTO> GetPost(int id)
		{
			Post post = (await Database.Posts.Find(x => x.Id.Equals(id))).FirstOrDefault();
			if(post != null)
			{
				PostDTO postDto = new PostDTO()
				{
					Id = post.Id,
					Content = post.Content,
					Title = post.Title,
				};
				return postDto;
			}
			else
			{
				throw new ServiceException("Post is not found", "post");
			}
		}

		public async Task EditPost(PostDTO postDto)
		{
			if (postDto != null)
			{
				var post = (await Database.Posts.Find(x => x.Id.Equals(postDto.Id))).FirstOrDefault();
				if (post != null)
				{
					post.Content = postDto.Content ?? post.Content;
					post.Title = postDto.Title ?? post.Title;
					post.IsDeleted = postDto.IsDeleted;
					await Database.Posts.Update(post);
					await Database.SaveAsync();
				}
			}
			else
			{
				throw new ServiceException("Post is not found", "postDto");
			}
		}

		/// <summary>
		/// Returns all posts
		/// </summary>
		/// <returns>List of PostDTO</returns>
		public async Task<IEnumerable<PostDTO>> GetPosts()
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>()
						.ForMember("OwnerId", opt => opt.MapFrom(p => p.ClientProfile.Id)))
						.CreateMapper();
			return mapper.Map<IEnumerable<Post>, List<PostDTO>>(await Database.Posts.GetAll()).ToList();
		}

		public void Dispose()
		{
			Database.Dispose();
		}
	}
}

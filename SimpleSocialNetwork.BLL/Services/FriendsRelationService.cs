﻿using AutoMapper;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Services
{
	public class FriendsRelationService : IFriendsRelationService
	{
		IUnitOfWork Database { get; set; }
		public FriendsRelationService(IUnitOfWork uow)
		{
			Database = uow;
		}

		/// <summary>
		/// Adds new friend relations between two users
		/// </summary>
		/// <param name="profile">First user</param>
		/// <param name="friend">Second user</param>
		/// <returns>Details about operation</returns>
		public async Task<OperationDetails> AddFriend(UserDTO profile, UserDTO friend)
		{
			if (profile != null && friend != null)
			{
				FriendsRelations relation = new FriendsRelations();
				relation.UserId = profile.Id;
				relation.FriendId = friend.Id;

				FriendsRelations reverseRelation = new FriendsRelations();
				reverseRelation.FriendId = profile.Id;
				reverseRelation.UserId = friend.Id;

				await Database.FriendsRelations.Create(relation);
				await Database.FriendsRelations.Create(reverseRelation);
				await Database.SaveAsync();
				return new OperationDetails(true, "User is added", "");
			}
			else if (profile == null)
			{
				return new OperationDetails(false, "Profile was null", "profile");
			}
			else
			{
				return new OperationDetails(false, "Friend was null", "friend");
			}
		}

		/// <summary>
		/// Removes friend relation between two users
		/// </summary>
		/// <param name="profile">First user</param>
		/// <param name="friend">Second user</param>
		/// <returns>Details about operation</returns>
		public async Task<OperationDetails> RemoveFriend(UserDTO profile, UserDTO friend)
		{
			if(profile != null && friend != null)
			{
				FriendsRelations relation = (await Database.FriendsRelations.Find(x => x.UserId.Equals(profile.Id)))
					.FirstOrDefault(x => x.FriendId.Equals(friend.Id));
				FriendsRelations reverseRelation = (await Database.FriendsRelations.Find(x => x.UserId.Equals(friend.Id)))
					.FirstOrDefault(x => x.FriendId.Equals(profile.Id));

				await Database.FriendsRelations.Delete(relation.Id);
				await Database.FriendsRelations.Delete(reverseRelation.Id);
				await Database.SaveAsync();
				return new OperationDetails(true, "User is removed", "");
			}
			else if(profile == null)
			{
				return new OperationDetails(false, "Profile was null", "profile");
			}
			else
			{
				return new OperationDetails(false, "Friend was null", "friend");
			}
		}

		/// <summary>
		/// Gets all user friends relations
		/// </summary>
		/// <param name="user">User relation to get</param>
		/// <returns>List of FriendshipDTO</returns>
		public async Task<IEnumerable<FriendshipDTO>> GetUserFriends(UserDTO user)
		{
			if(user != null)
			{
				var mapper = new MapperConfiguration(cfg => cfg.CreateMap<FriendsRelations, FriendshipDTO>()).CreateMapper();
				return mapper.Map<IEnumerable<FriendsRelations>, List<FriendshipDTO>>((await Database.FriendsRelations.GetAll()).Where(x => x.UserId.Equals(user.Id)));
			}
			return new List<FriendshipDTO>();
		}
	}
}

﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using SimpleSocialNetwork.DAL.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SimpleSocialNetwork.BLL.Services
{
	public class UserService : IUserService
	{
		IUnitOfWork Database { get; set; }

		public UserService(IUnitOfWork uow)
		{
			Database = uow;
		}

		/// <summary>
		/// Creates new user in system
		/// </summary>
		/// <param name="user">User to create</param>
		/// <returns>Details of opertaion</returns>
		public async Task<OperationDetails> CreateUser(UserDTO user)
		{
			AppUser appUser = await Database.UserManager.FindByEmailAsync(user.Email);
			if(appUser == null)
			{
				appUser = new AppUser { Email = user.Email, UserName = user.Email };
				var result = await Database.UserManager.CreateAsync(appUser, user.Password);
				if(result.Errors.Any())
				{
					return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
				}
				await Database.UserManager.AddToRoleAsync(appUser.Id, user.Role);

				ClientProfile profile = new ClientProfile()
				{
					Id = appUser.Id,
					Name = user.Name,
					CreatedAt = DateTime.Now,
					BirthdayDate = user.BirthdayDate,
					IsDeleted = false,
				};
				await Database.ClientProfiles.Create(profile);
				await Database.SaveAsync();
				return new OperationDetails(true, "Registration complete!", "");
			}
			else
			{
				return new OperationDetails(false, "User with this email already existed", "Email");
			}
		}
		
		/// <summary>
		/// Authenticate user in system
		/// </summary>
		/// <param name="user">User to authenticate</param>
		/// <returns>New ClaimsIdentity</returns>
		public async Task<ClaimsIdentity> Authenticate(UserDTO user)
		{
			ClaimsIdentity claim = null;
			AppUser appUser = await Database.UserManager.FindAsync(user.Email, user.Password);
			if(appUser != null)
			{
				var profile = (await Database.ClientProfiles.Find(x => x.Id.Equals(appUser.Id))).FirstOrDefault();
				if(profile != null && profile.IsDeleted)
				{
					return claim;
				}
				claim = await Database.UserManager.CreateIdentityAsync(appUser, DefaultAuthenticationTypes.ApplicationCookie);
			}
			return claim;
		}

		/// <summary>
		/// Set initial data to database
		/// </summary>
		/// <param name="admin">Admin profile to set</param>
		/// <param name="roles">Base roles to set</param>
		/// <returns></returns>
		public async Task SetInitialData(UserDTO admin, List<string> roles)
		{
			foreach(string roleName in roles)
			{
				var role = Database.RoleManager.FindByNameAsync(roleName);
				if(role == null)
				{
					AppRole appRole = new AppRole { Name = roleName };
					await Database.RoleManager.CreateAsync(appRole);
				}
			}
			await CreateUser(admin);
		}

		public async Task<UserDTO> GetUser(string id)
		{
			if(id == null)
			{
				throw new ValidationException("No id is set for user!", "id");
			}

			ClientProfile profile = (await Database.ClientProfiles.GetAll()).FirstOrDefault(x => x.Id.Equals(id));
			if(profile == null)
			{
				throw new ValidationException("User with this id doesn't exist!", "profile");
			}

			var config = new MapperConfiguration(cfg => cfg.CreateMap<ClientProfile, UserDTO>()
				.ForMember(x => x.Email, opt => opt.MapFrom(p => p.AppUser.Email)));

			var mapper = new Mapper(config);
			UserDTO user = mapper.Map<ClientProfile, UserDTO>(profile);

			return user;
		}

		public async Task UpdateUser(UserDTO user)
		{
			if(user != null)
			{
				ClientProfile profile = (await Database.ClientProfiles.GetAll()).FirstOrDefault(x => x.Id.Equals(user.Id));
				if(profile != null)
				{
					profile.Name = user.Name ?? profile.Name;
					profile.Status = user.Status ?? profile.Status;
					profile.Avatar = user.Avatar ?? profile.Avatar;
					profile.About = user.About ?? profile.About;
					profile.Location = user.Location ?? profile.Location;
					profile.BirthdayDate = user.BirthdayDate;
					profile.IsDeleted = user.IsDeleted;

					if(user.Password != null)
					{
						await Database.UserManager.RemovePasswordAsync(profile.Id);
						await Database.UserManager.AddPasswordAsync(profile.Id, user.Password);
					}
					if(user.Email != null)
					{
						await Database.UserManager.SetEmailAsync(profile.Id, user.Email);
					}

					await Database.ClientProfiles.Update(profile);
					await Database.SaveAsync();
				}
			}
			else
			{
				throw new ServiceException("User is not found", "user");
			}
		}

		/// <summary>
		/// Get all users for friend view
		/// </summary>
		/// <returns>List of UserDTO</returns>
		public async Task<IEnumerable<UserDTO>> GetUsers()
		{
			var clientProfiles = await Database.ClientProfiles.GetAll();
			if (clientProfiles != null)
			{
				var config = new MapperConfiguration(cfg => cfg.CreateMap<ClientProfile, UserDTO>()
					.ForMember(x => x.Email, opt => opt.MapFrom(p => p.AppUser.Email)));

				var mapper = new Mapper(config);
				var users = mapper.Map<List<UserDTO>>(clientProfiles);
				return users;
			}
			else
			{
				throw new ServiceException("There is no client profiles", "clientProfiles");
			}
		}

		/// <summary>
		/// Get all user messages where id equals parameter
		/// </summary>
		/// <param name="id">Id of user to get messages</param>
		/// <returns>List of MessageDTO</returns>
		public async Task<IEnumerable<MessageDTO>> GetUserMessages(string id)
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Message, MessageDTO>()).CreateMapper();
			return mapper.Map<IEnumerable<Message>, List<MessageDTO>>((await Database.Messages.GetAll()).Where(x => x.ClientProfile.Id.Equals(id)));
		}

		public void Dispose()
		{
			Database.Dispose();
		}
	}
}

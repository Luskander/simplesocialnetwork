﻿using AutoMapper;
using SimpleSocialNetwork.BLL.DTO;
using SimpleSocialNetwork.BLL.Infrastructure;
using SimpleSocialNetwork.BLL.Interfaces;
using SimpleSocialNetwork.DAL.Entities;
using SimpleSocialNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Services
{
	public class MessageService : IMessageService
	{
		IUnitOfWork Database { get; set; }

		public MessageService(IUnitOfWork uow)
		{
			Database = uow;
		}

		public async Task SendMessage(MessageDTO message)
		{
			if(message != null)
			{
				Message mes = new Message()
				{
					ClientProfile = (await Database.ClientProfiles.Find(x => x.Id.Equals(message.SenderId))).FirstOrDefault(),
					SenderId = message.SenderId,
					Content = message.Content,
					CreatedAt = message.CreatedAt,
					IsDeleted = message.IsDeleted,
					ReceiverId = message.ReceiverId,
				};
				await Database.Messages.Create(mes);
				await Database.SaveAsync();
			}
			else
			{
				throw new ArgumentNullException("message", "Message is null");
			}
		}

		public async Task DeleteMessage(MessageDTO message)
		{
			var messages = await Database.Messages.GetAll();
			Message mes = (await Database.Messages.Find(x => x.Id.Equals(message.Id))).FirstOrDefault();
			if(mes != null)
			{
				mes.IsDeleted = true;
				await Database.SaveAsync();
			}
			else
			{
				throw new ServiceException("There is no such message to delete", "message");
			}
		}

		public async Task EditMessage(MessageDTO message)
		{
			if(message != null)
			{
				var mes = (await Database.Messages.Find(x => x.Id.Equals(message.Id))).FirstOrDefault();
				if(mes != null)
				{
					mes.Content = message.Content ?? mes.Content;
					mes.IsDeleted = message.IsDeleted;
					await Database.Messages.Update(mes);
					await Database.SaveAsync();
				}
				else
				{
					throw new ServiceException("There is no such message to edit", "message");
				}
			}
			else
			{
				throw new ServiceException("Message is null", "message");
			}
		}

		/// <summary>
		/// Returns user message based on userId
		/// </summary>
		/// <param name="userId"></param>
		/// <returns>List of MessageDTO</returns>
		public async Task<IEnumerable<MessageDTO>> GetUserMessages(string userId)
		{
			if(userId != null)
			{
				var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Message, MessageDTO>()).CreateMapper();
				return mapper.Map<IEnumerable<Message>, List<MessageDTO>>((await Database.Messages.GetAll()).Where(x => x.SenderId.Equals(userId)));
			}
			else
			{
				throw new ServiceException("There is no messages for this user", "userId");
			}
		}

		/// <summary>
		/// Returns all messages for Admin
		/// </summary>
		/// <returns>List of MessageDTO</returns>
		public async Task<IEnumerable<MessageDTO>> GetMessages()
		{
			var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Message, MessageDTO>()).CreateMapper();
			return mapper.Map<IEnumerable<Message>, List<MessageDTO>>(await Database.Messages.GetAll());
		}

		public void Dispose()
		{
			Database.Dispose();
		}
	}
}

﻿using SimpleSocialNetwork.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.DTO
{
	public class UserDTO
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string Status { get; set; }
		public string Avatar { get; set; }
		public string Location { get; set; }
		public string About { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime BirthdayDate { get; set; }
		public bool IsDeleted { get; set; }
		public string Role { get; set; }

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Infrastructure
{
	/// <summary>
	/// Exception for throwing if user login or email does not valid
	/// </summary>
	public class ValidationException : Exception
	{
		public string Property { get; set; }
		public ValidationException(string message, string prop) : base(message)
		{
			Property = prop;
		}
	}
}

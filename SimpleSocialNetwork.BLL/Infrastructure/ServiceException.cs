﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Infrastructure
{
	/// <summary>
	/// Exception for throwing on error in services
	/// </summary>
	public class ServiceException : Exception
	{
		public string Property { get; set; }
		public ServiceException(string message, string prop) : base(message)
		{
			Property = prop;
		}
	}
}

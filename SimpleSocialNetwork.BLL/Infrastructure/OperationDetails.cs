﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Infrastructure
{
	/// <summary>
	/// Operation details for AuthorizeController actions
	/// </summary>
	public class OperationDetails
	{
		public bool Succeded { get; private set; }
		public string Message { get; private set; }
		public string Property { get; private set; }
		public OperationDetails(bool succeded, string message, string prop)
		{
			Succeded = succeded;
			Message = message;
			Property = prop;
		}
	}
}

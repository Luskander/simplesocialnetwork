﻿using Ninject.Modules;
using SimpleSocialNetwork.DAL.Interfaces;
using SimpleSocialNetwork.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetwork.BLL.Infrastructure
{
	/// <summary>
	/// Ninject module for bind IUnitOfWork to EFUnitOfWork
	/// </summary>
	public class ServiceModule : NinjectModule
	{
		private readonly string connectionString;
		public ServiceModule(string connection)
		{
			connectionString = connection;
		}

		public override void Load()
		{
			Bind<IUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(connectionString);
		}
	}
}
